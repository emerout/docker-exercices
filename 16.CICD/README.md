## Agenda

Discover how to setup a continuous integration with Gitlab CI and Docker and deploy your application in a Swarm cluster

## Prerequisites

A laptop with:
- Docker, you can get it from the [Docker Hub](https://hub.docker.com)
- Git

An account on https://gitlab.com, your public key deployed.

## Steps

[1. Build a web server using your favorite language](./01-WebServer)  

[2. Add a Dockerfile](./02-Docker)  

[3. Create a Gitlab repository](./03-Gitlab)  

[4. Setup Swarm](./04-Swarm)  

[5. Add a CICD pipeline](./05-CICD)  

[Let's start](./01-WebServer)
